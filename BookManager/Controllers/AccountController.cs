﻿using BookTool.Models;
using BookTool.Service;
using BookTool.ServiceImpl;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookManager.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        private readonly IUserService userService = new UserServiceImpl();
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Account");
        }
        [HttpGet]
        public ActionResult Account()
        {
            return View();
        }

        #region login && register
        public JsonResult LoginFunction(User user)
        {
            if (user == null)
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            string databaseError = null;

            User result = userService.GetUser(user, out databaseError);

            if (result == null)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

                return Json(Constants.Succes.NO_CONTENT_CODE + "," + "The user dosen't exist!", JsonRequestBehavior.AllowGet);
            }

            if (result.Password.Equals(user.Password))
            {
                Session["Email"] = user.Email;
                return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
            }
            return Json(Constants.Error.INFORMATION_CODE + ",Invalid password!", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegisterFunction(User user)
        {
            if (user == null)
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            string databaseError = null;

            if (Constants.DataBaseOperation.IsAlreadyInsert("SELECT * FROM T_USER WHERE EMAIL='" + user.Email + "' AND USERNAME='" + user.Username + "' AND PASSWORD='" + user.Password + "'", out databaseError) == true)
            {
                return Json(Constants.Error.INFORMATION_CODE + "," + "The user already exist!", JsonRequestBehavior.AllowGet);
            }

            if (String.IsNullOrEmpty(databaseError) && Constants.DataBaseOperation.IsAlreadyInsert("SELECT * FROM T_USER WHERE EMAIL='" + user.Email + "'", out databaseError) == true)
            {
                return Json(Constants.Error.INFORMATION_CODE + "," + "The email is take!", JsonRequestBehavior.AllowGet);
            }

            if (!String.IsNullOrEmpty(databaseError) || userService.InsertUser(user, out databaseError) == false)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);
            }

            Session["Email"] = user.Email;

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}