﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IBaseRepository<T>
    {
        bool Update(string sql, out string databaseError);
    }
}
