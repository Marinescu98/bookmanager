﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IUserRepository
    {
        User GetUser(User user, out string databaseError);
        bool InsertUser(User user, out string databaseError);
    }
}
