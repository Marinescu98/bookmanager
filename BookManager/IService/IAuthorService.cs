﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IService
{
    public interface IAuthorService
    {
        List<Author> SelectAuthors(out string databaseError);
    }
}
