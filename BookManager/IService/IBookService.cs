﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookTool.Service
{
    public interface IBookService
    {
        bool InsertBook(Book book, out string databaseError);
        List<Book> SelectBook(out string databaseError);
        bool UpdateBook(Book book, out string databaseError);
    }
}
