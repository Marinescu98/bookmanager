﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookTool.Service
{
    public interface IGenreService
    {

        List<Genre> SelectGenre(out string databaseError);

    }
}
