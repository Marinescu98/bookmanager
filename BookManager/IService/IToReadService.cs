﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IService
{
    public interface IToReadService
    {
        List<ToRead> SelectDataToRead(string email, out string databaseError);

        ToRead GetBook(int id, out string databaseError);

        bool InsertBookT_TOREAD(ToRead book,  out string databaseError);
        bool UpdateToRead(ToRead toRead, out string databaseError);
    }
}
