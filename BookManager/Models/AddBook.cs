﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.Models
{
    public class AddBook
    {
        public int Id { get; set; }
        public String FileName { get; set; }
        public byte[] FileContent { get; set; }
    }
}