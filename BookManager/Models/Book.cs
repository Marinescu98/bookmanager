﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.Models
{
    //update
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Author AuthorBook { get; set; }

        public string Edition { get; set; }

        public int Pages { get; set; }

        public Genre Category { get; set; }

        public string OnlineURL { get; set; }

    }
}