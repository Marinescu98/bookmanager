﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.Models
{
    public class ToRead
    {
        public int Id { get; set; }

        public Book BookAbout { get; set; }

        public string Comments { get; set; }

        public DateTime DateAdd { get; set; }

        public DateTime StartRead { get; set; }

        public DateTime FinishRead { get; set; }

        public User UserBook { get; set; }
    }
}