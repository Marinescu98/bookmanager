﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookManager.RepositoryImplementation
{
    public class AuthorRepository: IAuthorRepository
    {
        private SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING);
        public List<Author> SelectAuthors(out string databaseError)
        {
            databaseError = null;

            List<Author> authors = new List<Author>();

            try
            {
                string sql = "SELECT * FROM T_AUTHOR";

                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                authors.Add(
                                 new Author
                                 {
                                     ID = Int32.Parse(reader["ID"].ToString()),
                                     Name = reader["NAME"].ToString()
                                 });
                            }
                            return authors;
                        }
                        return null;
                    }
                }

            }
            catch (Exception exception)
            {
                databaseError = exception.ToString();
                return null;
            }
        }
    }
}