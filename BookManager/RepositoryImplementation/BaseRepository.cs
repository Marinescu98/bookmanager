﻿using BookManager.IRepository;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookManager.RepositoryImplementation
{
    public class BaseRepository<T>:IBaseRepository<T> where T: class
    {
        protected SqlConnection connection;
        public BaseRepository()
        {
            connection = new SqlConnection(Constants.CONNECTION_STRING);
        }
            
        public bool Update(string sql, out string databaseError)
        {
            databaseError = null;

            try
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    if (command.ExecuteNonQuery() > 0)
                    {
                        connection.Close();
                        return true;
                    }
                    else
                    {
                        connection.Close();
                        return false;
                    }
                }

            }
            catch (Exception exception)
            {
                databaseError = exception.Message;
                return false;
            }
        }
    }
}