﻿using BookManager.IRepository;
using BookManager.RepositoryImplementation;
using BookTool.Models;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookTool.Repository
{
    public class BookRepository :BaseRepository<Book>, IBookRepository
    {
        public bool InsertBook(Book book, out string databaseError)
        {
            databaseError = null;

            if (String.IsNullOrEmpty(book.OnlineURL))
                book.OnlineURL = "-";

            if (String.IsNullOrEmpty(book.Edition))
                book.Edition = "-";

            string sql;

            try
            {
                if (book.AuthorBook.ID == 0)
                {
                    if (book.Category.ID != 0)
                        sql = "INSERT INTO T_BOOK(TITLE, EDITION, ONLINE_URL, PAGES, ID_GENRE) " +
                                                      "VALUES('" + book.Title + "'," +
                                                      "'" +
                                                      book.Edition + "','" +
                                                      book.OnlineURL + "','" +
                                                      book.Pages + "','" +
                                                      book.Category.ID + "')";
                    else
                        sql = "INSERT INTO T_BOOK(TITLE, EDITION, ONLINE_URL, PAGES) " +
                                                      "VALUES('" + book.Title + "'," +
                                                      "'" +
                                                      book.Edition + "','" +
                                                      book.OnlineURL + "','" +
                                                      book.Pages + "')";
                }
                else
                {
                    if (book.Category.ID != 0)
                        sql = "INSERT INTO T_BOOK(TITLE, ID_AUTHOR, EDITION, ONLINE_URL, PAGES, ID_GENRE) " +
                                                      "VALUES('" + book.Title + "'," +
                                                      "'" + book.AuthorBook.ID + "','" +
                                                      book.Edition + "','" +
                                                      book.OnlineURL + "','" +
                                                      book.Pages + "','" +
                                                      book.Category.ID + "')";
                    else
                        sql = "INSERT INTO T_BOOK(TITLE,ID_AUTHOR, EDITION, ONLINE_URL, PAGES) " +
                                                      "VALUES('" + book.Title + "','" +
                                                      book.AuthorBook.ID + "','" +
                                                      book.Edition + "','" +
                                                      book.OnlineURL + "','" +
                                                      book.Pages + "')";

                }
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("TITLE", book.Title);
                    if (book.AuthorBook.ID != 0)
                        command.Parameters.AddWithValue("ID_AUTHOR", book.AuthorBook.ID);
                    if (book.Category.ID != 0)
                        command.Parameters.AddWithValue("ID_GENRE", book.Category.ID);
                    command.Parameters.AddWithValue("EDITION", book.Edition);
                    command.Parameters.AddWithValue("ONLINE_URL", book.OnlineURL);
                    command.Parameters.AddWithValue("PAGES", book.Pages);

                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }

            }
            catch (Exception expresion)
            {
                databaseError = expresion.ToString();
                return false;
            }
        }
        public List<Book> SelectBook(out string databaseError)
        {
            databaseError = null;
            List<Book> books = new List<Book>();

            try
            {
                string sql = "SELECT T_BOOK.ID as IdBook,T_BOOK.EDITION,T_BOOK.PAGES,T_BOOK.TITLE,T_BOOK.ONLINE_URL,ISNULL(T_AUTHOR.ID,'0') as IdAuthor,T_AUTHOR.NAME as nameA,ISNULL(T_GENRE.ID,'0') as IdGenre,T_GENRE.NAME as nameG FROM T_BOOK left JOIN T_AUTHOR ON T_BOOK.ID_AUTHOR=T_AUTHOR.ID left join T_GENRE on T_BOOK.ID_GENRE=T_GENRE.ID ";

                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                books.Add(new Book
                                {
                                    Id = Int32.Parse(reader["IdBook"].ToString()),
                                    Title = reader["TITLE"].ToString(),
                                    AuthorBook = new Author
                                    {
                                        ID = Int32.Parse(reader["IdAuthor"].ToString()),
                                        Name = reader["nameA"].ToString()
                                    },
                                    Edition = reader["EDITION"].ToString(),
                                    Category = new Genre
                                    {
                                        ID = Int32.Parse(reader["IdGenre"].ToString()),
                                        Name = reader["nameG"].ToString()
                                    },
                                    OnlineURL = reader["ONLINE_URL"].ToString(),
                                    Pages = Int32.Parse(reader["PAGES"].ToString())
                                });

                            }

                            reader.Close();
                            return books;

                        }

                        return null;
                    }
                }


            }
            catch (Exception expresion)
            {
                databaseError = expresion.ToString();
                return null;
            }
        }
    }
}