﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookManager.RepositoryImplementation
{
    public class ToReadRepository :BaseRepository<ToRead>, IToReadRepository
    {
        public ToReadRepository()
        {
            connection = new SqlConnection(Constants.CONNECTION_STRING);
        }
        public bool InsertBookT_TOREAD(ToRead book, out string databaseError)
        {
            databaseError = null;

            if (String.IsNullOrEmpty(book.Comments))
                book.Comments = "-";

            string sql = String.Empty;

            try
            {
                if (book.StartRead.ToString().Equals("1/1/0001 00:00:00"))
                    sql = "INSERT INTO T_TOREAD(ID_BOOK, COMMENTS,DATE_ADD, ID_USER) " +
                                                   "VALUES('" + book.Id + "','" +
                                                   book.Comments + "','" +
                                                   DateTime.Today.ToString() + "'," +
                                                   "(SELECT ID FROM T_USER WHERE EMAIL='" + book.UserBook.Email + "'))";
                else
                    sql = "INSERT INTO T_TOREAD(ID_BOOK, COMMENTS,DATE_ADD,START_READ, ID_USER) " +
                                                    "VALUES('" + book.Id + "','" +
                                                    book.Comments + "'," + "'" +
                                                    DateTime.Today.ToString() + "','" +
                                                    book.StartRead + "'," +
                                                    "(SELECT ID FROM T_USER WHERE EMAIL='" + book.UserBook.Email + "'))";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("ID_BOOK", book.Id);
                    command.Parameters.AddWithValue("DATE_ADD", DateTime.Today.ToString());
                    if (!book.StartRead.ToString().Equals("1/1/0001 00:00:00"))
                        command.Parameters.AddWithValue("START_READ", book.StartRead);
                    command.Parameters.AddWithValue("COMMENTS", book.Comments);
                    command.Parameters.AddWithValue("EMAIL", book.UserBook.Email);

                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
            }
            catch (Exception expresion)
            {
                databaseError = expresion.ToString();
                return false;
            }
        }
        public List<ToRead> Select(string email, out string databaseError)
        {
            databaseError = null;
            List<ToRead> books = new List<ToRead>();

            try
            {
                string sql = "SELECT T_BOOK.ID as IdBook," +
                    "T_BOOK.EDITION," +
                    "T_BOOK.PAGES," +
                    "T_BOOK.TITLE," +
                    "T_BOOK.ONLINE_URL," +
                    "ISNULL(T_AUTHOR.ID, '0') as IdAuthor," +
                    "T_AUTHOR.NAME as nameA," +
                    "ISNULL(T_GENRE.ID, '0') as IdGenre," +
                    "T_GENRE.NAME as nameG," +
                    "T_TOREAD.COMMENTS," +
                    "ISNULL(T_TOREAD.START_READ,'') START_READ," +
                    "ISNULL(T_TOREAD.DATE_ADD,'') DATE_ADD," +
                    "ISNULL(T_TOREAD.FINISH_READ,'') FINISH_READ " +
                    "FROM T_BOOK " +
                    "left JOIN T_AUTHOR ON T_BOOK.ID_AUTHOR = T_AUTHOR.ID " +
                    "left join T_GENRE on T_BOOK.ID_GENRE = T_GENRE.ID " +
                    "right join T_TOREAD on T_TOREAD.ID_BOOK = T_BOOK.ID " +
                    "where T_TOREAD.ID_USER = (select id from T_USER where T_USER.EMAIL = '" + email + "')";

                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                books.Add(new ToRead
                                {
                                    Id = Int32.Parse(reader["IdBook"].ToString()),
                                    BookAbout = new Book
                                    {
                                        Title = reader["TITLE"].ToString(),
                                        AuthorBook = new Author
                                        {
                                            ID = Int32.Parse(reader["IdAuthor"].ToString()),
                                            Name = reader["nameA"].ToString()
                                        },
                                        Edition = reader["EDITION"].ToString(),
                                        OnlineURL = reader["ONLINE_URL"].ToString(),
                                        Pages = Int32.Parse(reader["PAGES"].ToString()),
                                        Category = new Genre
                                        {
                                            ID = Int32.Parse(reader["IdGenre"].ToString()),
                                            Name = reader["nameG"].ToString()
                                        }
                                    },
                                    Comments = reader["COMMENTS"].ToString(),
                                    DateAdd = DateTime.Parse(reader["DATE_ADD"].ToString()),
                                    StartRead = DateTime.Parse(reader["START_READ"].ToString()),
                                    FinishRead = DateTime.Parse(reader["FINISH_READ"].ToString())

                                });

                            }

                            reader.Close();
                            return books;

                        }

                        return null;
                    }
                }


            }
            catch (Exception expresion)
            {
                databaseError = expresion.ToString();
                return null;
            }
        }
        public ToRead GetBook(int id,  out string databaseError)
        {
            databaseError = null;

            try
            {
                string sql = "SELECT T_BOOK.EDITION," +
                    "T_BOOK.PAGES," +
                    "T_BOOK.TITLE," +
                    "T_BOOK.ONLINE_URL," +
                    "ISNULL(T_AUTHOR.ID, '0') as IdAuthor," +
                    "T_AUTHOR.NAME as nameA," +
                    "ISNULL(T_GENRE.ID, '0') as IdGenre," +
                    "T_GENRE.NAME as nameG," +
                    "T_TOREAD.COMMENTS," +
                    "ISNULL(T_TOREAD.START_READ,'') START_READ," +
                    "ISNULL(T_TOREAD.DATE_ADD,'') DATE_ADD," +
                    "ISNULL(T_TOREAD.FINISH_READ,'') FINISH_READ " +
                    "FROM T_BOOK " +
                    "left JOIN T_AUTHOR ON T_BOOK.ID_AUTHOR = T_AUTHOR.ID " +
                    "left join T_GENRE on T_BOOK.ID_GENRE = T_GENRE.ID " +
                    "left join T_TOREAD on T_TOREAD.ID_BOOK = T_BOOK.ID  " +
                    "where T_TOREAD.ID='" + id + "'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                return new ToRead
                                {
                                    BookAbout = new Book
                                    {
                                        AuthorBook = new Author
                                        {
                                            ID = Int32.Parse(reader["IdAuthor"].ToString()),
                                            Name = reader["nameA"].ToString()
                                        },
                                        Edition = reader["EDITION"].ToString(),
                                        Category = new Genre
                                        {
                                            ID = Int32.Parse(reader["IdGenre"].ToString()),
                                            Name = reader["nameG"].ToString()
                                        },
                                        OnlineURL = reader["ONLINE_URL"].ToString(),
                                        Pages = Int32.Parse(reader["PAGES"].ToString()),
                                        Title = reader["TITLE"].ToString(),
                                        Id = id
                                    },

                                    Comments = reader["COMMENTS"].ToString(),
                                    DateAdd = DateTime.Parse(reader["DATE_ADD"].ToString()),
                                    StartRead = DateTime.Parse(reader["START_READ"].ToString()),
                                    FinishRead = DateTime.Parse(reader["FINISH_READ"].ToString()),
                                    Id=id
                                    
                                };
                            }
                        }
                        return null;
                    }
                }

            }
            catch (Exception e)
            {
                databaseError = e.Message;
                return null;
            }
        }
    }
}