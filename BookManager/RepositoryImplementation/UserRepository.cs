﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookTool.Repository
{
    public class UserRepository : IUserRepository
    {
        private SqlConnection connection;
        public UserRepository()
        {
            connection = new SqlConnection(Constants.CONNECTION_STRING);
        }
        public User GetUser(User user, out string databaseError)
        {
            databaseError = null;

            try
            {
                string sql = "SELECT * FROM T_USER WHERE EMAIL='" + user.Email + "'";

                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                return new User
                                {
                                    Email = reader["EMAIL"].ToString(),
                                    Username = reader["USERNAME"].ToString(),
                                    Password = reader["PASSWORD"].ToString()
                                };
                            }
                        }
                        reader.Close();
                        return null;
                    }
                }

            }
            catch (Exception exception)
            {
                databaseError = exception.ToString();
                return null;
            }
        }

        public bool InsertUser(User user, out string databaseError)
        {
            databaseError = null;

            try
            {
                string sql = "INSERT INTO T_USER(USERNAME,EMAIL,PASSWORD) VALUES('" + user.Username + "','" + user.Email + "','" + user.Password + "')";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();

                    command.Parameters.AddWithValue("USERNAME", user.Username);
                    command.Parameters.AddWithValue("EMAIL", user.Email);
                    command.Parameters.AddWithValue("PASSWORD", user.Password);

                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }

            }
            catch (Exception exception)
            {
                databaseError = exception.ToString();
                return false;
            }
        }
    }
}