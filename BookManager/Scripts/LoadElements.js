﻿function PopulateDropDownList(init, name,nameText,url) {
    $.ajax({
        type: 'get',
        url: url,
        success: function (result) {
            var mesRespDropDown = document.getElementById(nameText);
            var option = document.createElement('option');

            mesRespDropDown.options.length = 0;
            option.value = init;
            option.text = name;
            mesRespDropDown.appendChild(option);

            for (var i = 0; i < result.length; i++) {
                var option = document.createElement('option');
                option.value = result[i].ID;
                option.text = result[i].Name;
                mesRespDropDown.appendChild(option);
            }
        }
    });

}

function PopulateDropDownListBook() {
    $.ajax({
        type: 'get',
        url: 'DropDownListBook',
        success: function (result) {
            var mesRespDropDown = document.getElementById('BookDDL');
            var option = document.createElement('option');

            mesRespDropDown.options.length = 0;
            option.value = "0";
            option.text = "Books";
            mesRespDropDown.appendChild(option);

            for (var i = 0; i < result.length; i++) {
                var option = document.createElement('option');
                option.value = result[i].Id;
                option.text = result[i].Title + ' ' + result[i].AuthorBook.Name;
                mesRespDropDown.appendChild(option);
            }
        }
    });

}