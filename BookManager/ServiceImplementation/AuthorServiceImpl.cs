﻿using BookManager.IRepository;
using BookManager.IService;
using BookManager.RepositoryImplementation;
using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookManager.ServiceImplementation
{
    public class AuthorServiceImpl:IAuthorService
    {
        private IAuthorRepository authorRepository = new AuthorRepository();
        public List<Author> SelectAuthors(out string databaseError)
        {
            return authorRepository.SelectAuthors(out databaseError);
        }
    }
}