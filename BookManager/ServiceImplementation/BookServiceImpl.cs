﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Repository;
using BookTool.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.ServiceImpl
{
    public class BookServiceImpl : IBookService
    {
        private IBookRepository bookRepository = new BookRepository();
        public bool InsertBook(Book book, out string databaseError)
        {
            return bookRepository.InsertBook(book, out databaseError);
        }
        public List<Book> SelectBook(out string databaseError)
        {
            return bookRepository.SelectBook(out databaseError);
        }
        public bool UpdateBook(Book book, out string databaseError)
        {
            string sql= "update Book set TITLE='"
                + book.Title + "', ID_AUTHOR="
                + book.AuthorBook.ID + ", EDITION='"
                + book.Edition + "', ONLINE_URL='" +
                book.OnlineURL + "',ID_GENRE="+book.Category.ID+",PAGES="+book.Pages+" where ID=" + book.Id + "";

            return bookRepository.Update(sql, out databaseError);
        }
    }
}