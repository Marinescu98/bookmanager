﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Repository;
using BookTool.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.ServiceImpl
{
    public class GenreServiceImpl : IGenreService
    {
        private IGenreRepository genreRepository = new GenreRepository();
        public List<Genre> SelectGenre(out string databaseError)
        {
            return genreRepository.SelectGenre(out databaseError);
        }

    }
}