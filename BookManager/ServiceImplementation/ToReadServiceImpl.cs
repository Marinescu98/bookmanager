﻿using BookManager.IRepository;
using BookManager.IService;
using BookManager.RepositoryImplementation;
using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookManager.ServiceImplementation
{
    public class ToReadServiceImpl: IToReadService
    {
        private IToReadRepository toReadRepository = new ToReadRepository();
        public ToRead GetBook(int id, out string databaseError)
        {
            return toReadRepository.GetBook(id, out databaseError);
        }
        public bool InsertBookT_TOREAD(ToRead book, out string databaseError)
        {
            return toReadRepository.InsertBookT_TOREAD(book, out databaseError);
        }

        public List<ToRead> SelectDataToRead(string email, out string databaseError)
        {
            return toReadRepository.Select(email, out databaseError);
        }

        public bool UpdateToRead(ToRead toRead, out string databaseError)
        {
            string sql = "update ToRead set COMMENTS='"
                + toRead.Comments + "' where Id=" + toRead.Id + "";
            return toReadRepository.Update(sql, out databaseError);
        }
    }
}