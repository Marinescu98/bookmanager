﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Repository;
using BookTool.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookTool.ServiceImpl
{
    public class UserServiceImpl : IUserService
    {
        private readonly IUserRepository userRepository = new UserRepository();
        public User GetUser(User user, out string databaseError)
        {
            return userRepository.GetUser(user,out databaseError);
        }

        public bool InsertUser(User user, out string databaseError)
        {
            return userRepository.InsertUser(user,out databaseError);
        }
    }
}