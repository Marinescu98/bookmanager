﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookTool.Utility
{
    public class Constants
    {
        public static string CONNECTION_STRING = System.Configuration.ConfigurationManager.ConnectionStrings["BookDB"].ToString();

        public class Error
        {
            public static string DATABASE_ERROR_CODE = "500";

            public static string INFORMATION_CODE = "100";
            public static string INFORMATION_MESSAGE = "The data are already insert!";

            public static string BAD_REQUEST_CODE = "400";
            public static string BAD_REQUEST_MESSAGE = "Bad request!";

            public static string REDIRECT_CODE = "300";
            public static string REDIRECT_MESSAGE = "Redirect!";
        }
        public class Succes
        {
            public static string SUCCESS_CODE = "200";
            public static string SUCCESS_MESSAGE = "Success!";

            public static string NO_CONTENT_CODE = "204";
            public static string NO_CONTENT_MESSAGE = "No content!";//nu exita contitnut dar requestul a fost primit
        }

        public class DataBaseOperation
        {

            public static bool IsAlreadyInsert(string sql, out string databaseError)
            {
                databaseError = null;

                if (String.IsNullOrEmpty(sql))
                    return false;

                try
                {
                    using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    reader.Close();
                                    return true;
                                }
                                else
                                {
                                    reader.Close();
                                    return false;
                                }
                            }
                        }
                    }

                }
                catch (Exception expresion)
                {
                    databaseError = expresion.ToString();
                    return false;
                }
            }
            public static string SelectOneFieldById(string sql, string name, out string databaseError)
            {
                databaseError = null;

                if (String.IsNullOrEmpty(sql))
                    return null;

                try
                {
                    using (SqlConnection connection = new SqlConnection(Constants.CONNECTION_STRING))
                    {
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                        return reader[name].ToString();
                                }

                                reader.Close();
                                return null;
                            }
                        }
                    }

                }
                catch (Exception expresion)
                {
                    databaseError = expresion.ToString();
                    return null;
                }
            }
            public static bool InsertOneField(string insert,string name,string sql, out string databaseError)
            {
                databaseError = null;

                if (String.IsNullOrEmpty(sql))
                    return false;

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(Constants.CONNECTION_STRING))
                    {                       

                        using (SqlCommand command = new SqlCommand(sql, sqlConnection))
                        {
                            sqlConnection.Open();

                            command.Parameters.AddWithValue(name, insert);

                            command.ExecuteNonQuery();
                            sqlConnection.Close();
                            return true;
                        }
                    }

                }
                catch (Exception expresion)
                {
                    databaseError = expresion.Message;
                    return false;
                }

            }
        }
    }


}